package no.uib.inf101.sample.many;

import no.uib.inf101.sample.single.ColorFieldModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ManyColorFieldsModel implements ViewableManyColorFields {
  private List<ColorFieldModel> fields = new ArrayList<>();

  /**
   * Creates a new model with n color fields.
   *
   * @param n     the number of fields
   * @param random a random number generator
   */
  public ManyColorFieldsModel(int n, Random random) {
    for (int i = 0; i < n; i++) {
      fields.add(new ColorFieldModel(random));
    }
  }

  @Override
  public Iterable<ColorFieldModel> getFields() {
    return this.fields;
  }
}
