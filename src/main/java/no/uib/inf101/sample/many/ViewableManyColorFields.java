package no.uib.inf101.sample.many;

import no.uib.inf101.sample.single.ViewableColorField;

public interface ViewableManyColorFields {

  /** Gets the fields in this model. */
  Iterable<? extends ViewableColorField> getFields();
}
