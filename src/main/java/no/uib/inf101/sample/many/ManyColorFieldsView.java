package no.uib.inf101.sample.many;

import no.uib.inf101.sample.single.ColorFieldView;
import no.uib.inf101.sample.single.ViewableColorField;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class ManyColorFieldsView extends JPanel {

  private Map<ViewableColorField, ColorFieldView> views = new HashMap<>();

  /** Creates a new view for the given model. */
  public ManyColorFieldsView(ViewableManyColorFields model) {
    for (ViewableColorField field : model.getFields()) {
      ColorFieldView subPanel = new ColorFieldView(field);
      this.views.put(field, subPanel);
      this.add(subPanel);
    }
  }

  /** Gets the view for the given model. */
  public ColorFieldView getViewForField(ViewableColorField model) {
    return this.views.get(model);
  }
}
