package no.uib.inf101.sample.many;

import javax.swing.*;
import java.util.Random;

public class MainMany {
  public static void main(String[] args) {
    ManyColorFieldsModel model = new ManyColorFieldsModel(10, new Random());
    ManyColorFieldsView view = new ManyColorFieldsView(model);
    new ManyColorFieldsController(model, view);

    JFrame frame = new JFrame("Many color fields");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
