package no.uib.inf101.sample.many;

import no.uib.inf101.sample.single.ColorFieldController;
import no.uib.inf101.sample.single.ColorFieldModel;
import no.uib.inf101.sample.single.ColorFieldView;

public class ManyColorFieldsController {

  /** Creates a controller for the given model and view. */
  public ManyColorFieldsController(ManyColorFieldsModel model, ManyColorFieldsView view) {
    for (ColorFieldModel field : model.getFields()) {
      ColorFieldView fieldView = view.getViewForField(field);
      new ColorFieldController(field, fieldView);
    }
  }
}
