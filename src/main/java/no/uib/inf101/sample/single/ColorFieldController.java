package no.uib.inf101.sample.single;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

public class ColorFieldController extends MouseAdapter implements ActionListener {

  private ColorFieldModel model;
  private ColorFieldView view;

  /** Create a new controller for the given model and view. */
  public ColorFieldController(ColorFieldModel model, ColorFieldView view) {
    this.model = model;
    this.view = view;
    this.view.addMouseListener(this);
    this.setupTimer();
  }

  @Override
  public void mousePressed(MouseEvent e) {
    super.mousePressed(e);
    Ellipse2D circle = this.view.getCircle();
    if (circle.contains(e.getPoint())) {
      this.model.disruptWithNewColor();
    };
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    this.model.goStep();
  }

  private void setupTimer() {
    Timer timer = new Timer(100, this);
    timer.start();
  }
}
