package no.uib.inf101.sample.single;

import no.uib.inf101.sample.eventbus.Event;

import java.awt.*;

/**
 * An event that is fired when the color of a color field model changes.
 */
public record ColorFieldModelChangedEvent(ColorFieldModel field, Color color) implements Event {
}
