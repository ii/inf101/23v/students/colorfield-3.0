package no.uib.inf101.sample.single;

import no.uib.inf101.sample.eventbus.EventHandler;

import java.awt.*;

public interface ViewableColorField {

  /**
   * Registers a listener for changes to the color field.
   * Whenever this object changes such that the result of
   * a call to getCurrentColor would return a different
   * value, the listener will be notified with a
   * ColorFieldModelChangedEvent.
   *
   * @param listener The listener to register.
   */
  void registerForEvents(EventHandler listener);

  /** Gets the current color of the field. */
  Color getCurrentColor();
}
