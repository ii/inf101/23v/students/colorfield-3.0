package no.uib.inf101.sample.single;

import javax.swing.JFrame;
import java.util.Random;

/**
 * Start the application. Creates the model, view and
 * controller, and shows the view in a window.
 */
public class MainSingle {

  /**
   * Start the application.
   *
   * @param args command line arguments (are ignored)
   */
  public static void main(String[] args) {
    ColorFieldModel model = new ColorFieldModel(new Random());
    ColorFieldView view = new ColorFieldView(model);
    new ColorFieldController(model, view);

    JFrame frame = new JFrame();
    frame.setTitle("INF101 Color Floater");
    frame.add(view);
    frame.pack();
    frame.setVisible(true);
  }
}
