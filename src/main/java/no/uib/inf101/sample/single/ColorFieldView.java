package no.uib.inf101.sample.single;

import no.uib.inf101.sample.eventbus.Event;
import no.uib.inf101.sample.eventbus.EventHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class ColorFieldView extends JComponent implements EventHandler {

  private ViewableColorField field;

  /** Create a new view for the given color field (model). */
  public ColorFieldView(ViewableColorField field) {
    this.field = field;
    this.field.registerForEvents(this);
    this.setPreferredSize(new Dimension(200, 200));
  }

  /** Gets the circle in which the color field is drawn. */
  public Ellipse2D getCircle() {
    return new Ellipse2D.Double(0, 0, this.getWidth(), this.getHeight());
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    Ellipse2D circle = this.getCircle();
    Color color = this.field.getCurrentColor();
    g2.setColor(color);
    g2.fill(circle);
  }

  @Override
  public void handle(Event event) {
    this.repaint();
  }
}
