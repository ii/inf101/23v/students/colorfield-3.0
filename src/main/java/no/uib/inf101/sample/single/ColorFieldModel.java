package no.uib.inf101.sample.single;

import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.eventbus.EventHandler;

import java.awt.*;
import java.util.Random;

public class ColorFieldModel implements ViewableColorField {

  private Color oldColor;
  private Color newColor;
  private EventBus bus;
  private Random random;
  private int step;

  private static final int NUMBER_OF_STEPS = 10;
  private static final int COLOR_VALUE_BOUND = 256;

  /** Create a new ColorFieldModel. */
  public ColorFieldModel(Random random) {
    this.random = random;
    this.bus = new EventBus();
    this.oldColor = this.generateRandomColor();
    this.newColor = this.generateRandomColor();
  }

  /** Sets the current color of the field to a new random color. */
  public void disruptWithNewColor() {
    this.oldColor = this.generateRandomColor();
    this.bus.post(new ColorFieldModelChangedEvent(this, this.oldColor));
  }

  private Color generateRandomColor() {
    return new Color(
        this.random.nextInt(COLOR_VALUE_BOUND),
        this.random.nextInt(COLOR_VALUE_BOUND),
        this.random.nextInt(COLOR_VALUE_BOUND)
    );
  }

  @Override
  public void registerForEvents(EventHandler listener) {
    this.bus.register(listener);
  }

  @Override
  public Color getCurrentColor() {
    return new Color(
        mix(this.oldColor.getRed(), this.newColor.getRed()),
        mix(this.oldColor.getGreen(), this.newColor.getGreen()),
        mix(this.oldColor.getBlue(), this.newColor.getBlue())
    );
  }

  public void goStep() {
    this.step++;
    if (this.step > NUMBER_OF_STEPS) {
      this.step = 0;
      this.oldColor = this.newColor;
      this.newColor = generateRandomColor();
    }
    this.bus.post(new ColorFieldModelChangedEvent(this, this.getCurrentColor()));
  }

  /**
   * Calculate the value which is "this.step" steps away from
   * oldValue, and NUMBER_OF_STEPS steps away from target value
   *
   * @param oldValue the old value
   * @param targetValue the target value
   * @return the value in between
   */
  private int mix(int oldValue, int targetValue) {
    return (targetValue * this.step + oldValue * (NUMBER_OF_STEPS - this.step)) / NUMBER_OF_STEPS;
  }
}
